# IDHub App

> IDHub member management website

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8081
$ npm run dev

# serve with hot reload at localhost:8081 with mock server
$ npm run mock

# build for production with minification
$ npm run build

# lint code
$ npm run lint
```
