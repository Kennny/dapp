import { Toast } from 'quasar'

export default {
  toastMessage (title, message, type) {
    let messageContent = ''
    let value = {
      html: '',
      bgColor: '#FFF',
      timeout: 3000
    }

    // setting toast content
    title = '<div style=\'font-size: 20px;\'>' + title + '</div>'
    messageContent = '<div style=\'font-size: 14px;\'>' + message + '</div>'

    value.html = title + messageContent
    // create toast
    Toast.create[type](value)
  }
}
