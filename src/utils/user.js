export default {
  getUserData (key) {
    var check = false
    if (localStorage.getItem('user')) {
      let user = JSON.parse(localStorage.getItem('user'))
      if (key) {
        if (user.hasOwnProperty(key)) {
          check = user[key]
        }
      }
      else {
        check = JSON.parse(localStorage.getItem('user'))
      }
    }
    return check
  },
  setUserData (user) {
    localStorage.setItem('user', JSON.stringify(user))
  },
  removeUserData () {
    if (localStorage.getItem('user')) {
      localStorage.removeItem('user')
    }
    if (localStorage.getItem('friendList')) {
      localStorage.removeItem('friendList')
    }
  }
}
