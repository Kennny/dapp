import axios from 'axios'
import Vue from 'vue'
import UserHandler from '@/utils/user'
export default {
  apiUrl () {
    return Vue.directive('baseApiUrl')
  },
  postRequest (url, params, resolve, reject) {
    return axios.post(this.apiUrl() + url, params)
      .then(res => resolve(res))
      .catch(err => reject(err))
  },
  userLogin (params, resolve, reject) {
    return this.postRequest('/user/login', params, resolve, reject)
  },
  userLogout (params, resolve, reject) {
    UserHandler.removeUserData()
  },
  userRegister (params, resolve, reject) {
    return this.postRequest('/user/register', params, resolve, reject)
  },
  searchFriend (params, resolve, reject) {
    return this.postRequest('/user/searchFriend', params, resolve, reject)
  },
  addFriend (params, resolve, reject) {
    return this.postRequest('/user/addFriend', params, resolve, reject)
  }
}
