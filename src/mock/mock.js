import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import Vue from 'vue'

export default {
  apiUrl () {
    return Vue.directive('baseApiUrl')
  },
  setUp () {
    let mock = new MockAdapter(axios, { delayResponse: 500 })

    /* Login */
    mock.onPost(this.apiUrl() + '/user/login').reply(config => {
      // console.log('data pass to in mock.js >> ' , config);
      let { account, password } = JSON.parse(config.data).data
      return new Promise((resolve, reject) => {
        if (account && password) {
          resolve([200, {
            status: 200,
            message: 'Success',
            error: {},
            data: {account, password}
          }])
        }
        else {
          reject(new Error('Error, account or password incorrect.'))
        }
      })
    })
    /* Register */
    mock.onPost(this.apiUrl() + '/user/register').reply(config => {
      let { account, password } = JSON.parse(config.data).data
      return new Promise((resolve, reject) => {
        if (account && password) {
          resolve([200, {
            status: 200,
            message: 'Success',
            error: {},
            data: {account, password}
          }])
        }
        else {
          reject(new Error('Register failed'))
        }
      })
    })
    /* Search Friend */
    mock.onPost(this.apiUrl() + '/user/searchFriend').reply(config => {
      let { email } = JSON.parse(config.data).data
      return new Promise((resolve, reject) => {
        if (email.includes('@')) {
          resolve([200, {
            status: 200,
            message: 'Success',
            error: {},
            data: {
              email: email,
              name: email.slice(0, email.indexOf('@'))
            }
          }])
        }
        else {
          reject(new Error('Search failed'))
        }
      })
    })
    /* Add Friend */
    mock.onPost(this.apiUrl() + '/user/addFriend').reply(config => {
      let { email } = JSON.parse(config.data).data
      return new Promise((resolve, reject) => {
        if (email.includes('@')) {
          resolve([200, {
            status: 200,
            message: 'Success',
            error: {},
            data: {
              email: email,
              name: email.slice(0, email.indexOf('@'))
            }
          }])
        }
        else {
          reject(new Error('Add failed'))
        }
      })
    })
  }
}
