import Vue from 'vue'
import Vuex from 'vuex'
// import custom module
import account from './account'

import getters from './getters'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    account
  },
  getters
})

export default store
