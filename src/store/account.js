import AccountHandler from '@/api/account'
import UserHandler from '@/utils/user'
const account = {
  state: {
    login: {
      loginState: false
    }
  },

  mutations: {
    'USER_LOGIN_SUCCESS': (state, {params, res}) => {
      console.log('res data :', res.data)
      if (res.status === 200) {
        state.login.loginState = true
        UserHandler.setUserData({account: res.data.data.account})
        params.resolve(res)
      }
      else {
        params.reject(res)
      }
    },
    'USER_REGISTER_SUCCESS': (state, {params, res}) => {
      console.log('res data :', res.data)
      if (res.status === 200) {
        params.resolve(res)
      }
      else {
        params.reject(res)
      }
    },
    'USER_ACTION_FAILED': (state, {params, err}) => {
      params.reject(err)
    }
  },

  actions: {
    UserLogin ({commit, state}, params) {
      AccountHandler.userLogin(params, res => {
        commit('USER_LOGIN_SUCCESS', {params, res})
      }, err => {
        commit('USER_ACTION_FAILED', {params, err})
      })
    },
    UserRegister ({commit, state}, params) {
      AccountHandler.userRegister(params, res => {
        commit('USER_REGISTER_SUCCESS', {params, res})
      }, err => {
        commit('USER_ACTION_FAILED', {params, err})
      })
    },
    SearchFriend ({commit, state}, params) {
      AccountHandler.searchFriend(params, res => {
        params.resolve(res)
      }, err => {
        params.reject(err)
      })
    },
    AddFriend ({commit, state}, params) {
      AccountHandler.addFriend(params, res => {
        params.resolve(res)
      }, err => {
        params.reject(err)
      })
    }
  }
}

export default account
